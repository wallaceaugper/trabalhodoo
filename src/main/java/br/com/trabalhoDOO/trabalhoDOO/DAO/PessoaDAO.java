package br.com.trabalhoDOO.trabalhoDOO.DAO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.trabalhoDOO.trabalhoDOO.model.Pessoa;
import br.com.trabalhoDOO.trabalhoDOO.utils.EntityManagerUtils;

public class PessoaDAO {

	private EntityManager entityManager;
	
	private EntityManagerUtils entityManagerUtils = new EntityManagerUtils();

	public void salvar(Pessoa pessoa) {
		entityManager = entityManagerUtils.getEntityManager();

		try {
			entityManager.getTransaction().begin();

			if (pessoa.getId() == null) {
				entityManager.persist(pessoa);
			} else {
				entityManager.merge(pessoa);
			}

			entityManager.getTransaction().commit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			entityManager.close();
		}
	}

	public void excluir(Pessoa pessoa) {
		entityManager = entityManagerUtils.getEntityManager();

		try {
			entityManager.getTransaction().begin();
			
			pessoa = entityManager.merge(pessoa);
			System.out.println("Excluindo os dados de: " + pessoa.getNome());

			entityManager.remove(pessoa);

			entityManager.getTransaction().commit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			entityManager.close();
		}
	}

	public Pessoa consultarPorId(Integer id) {
		entityManager = entityManagerUtils.getEntityManager();
		
		Pessoa pessoa = null;
		
		try {
			pessoa = entityManager.find(Pessoa.class, id);
			System.out.println(pessoa.toString());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			entityManager.close();
		}
		return pessoa;
	}
	
	@SuppressWarnings("unchecked")
	public List<Pessoa> consultarTodos() {
		try {
			entityManager = entityManagerUtils.getEntityManager();
			List<Pessoa> pessoas = new ArrayList<>();
			pessoas = entityManager.createQuery("FROM Pessoa").getResultList();
			System.out.println(pessoas.toString());
			
			return pessoas;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}
}
