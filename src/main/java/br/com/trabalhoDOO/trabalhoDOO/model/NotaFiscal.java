package br.com.trabalhoDOO.trabalhoDOO.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "NOTA_FISCAL")
public class NotaFiscal implements Serializable {
	
	private static final long serialVersionUID = 1356864670730893746L;

	@Id
	@Column(name = "idNotaFiscal", nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "numero", nullable = false, length = 45, unique = true)
	private int numero;
	
	@Column(name = "modelo", nullable = true)
	private String modelo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dtOperacao", nullable = false)
	private Date dataOperacao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dtEmissao", nullable = false)
	private Date dataEmissao;
	
	@Column(name="infComplementar", nullable = true, length = 2048)
	private String informacaoComplementar;

	@OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "notaFiscal", cascade = CascadeType.ALL)
	List<Item> itens = new ArrayList<Item>();
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "emitente", nullable = false)
	private Pessoa emitente;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "destinatario", nullable = false)
	private Pessoa destinatario;
	
	@Transient
	private int qtdItens;
	
	@Transient
	private double valor;

	public NotaFiscal(int numero, String modelo, Date dataOperacao, Date dataEmissao, String informacaoComplementar, int qtdItens, double valor, List<Item> itens) {
		this.numero = numero;
		this.modelo = modelo;
		this.dataOperacao = dataOperacao;
		this.dataEmissao = dataEmissao;
		this.informacaoComplementar = informacaoComplementar;
		this.qtdItens = getQtdItens();
		this.valor = getValor();
		this.itens = itens;
	}
	
	public NotaFiscal() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Date getDataOperacao() {
		return dataOperacao;
	}

	public void setDataOperacao(Date dataOperacao) {
		this.dataOperacao = dataOperacao;
	}

	public Date getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public Pessoa getEmitente() {
		return emitente;
	}

	public void setEmitente(Pessoa emitente) {
		this.emitente = emitente;
	}

	public Pessoa getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(Pessoa destinatario) {
		this.destinatario = destinatario;
	}

	public String getInformacaoComplementar() {
		return informacaoComplementar;
	}

	public void setInformacaoComplementar(String informacaoComplementar) {
		this.informacaoComplementar = informacaoComplementar;
	}

	public int getQtdItens() {
		return getItens().size();
	}

	public void setQtdItens(int qtdItens) {
		this.qtdItens = qtdItens;
	}

	public double getValor() {
		double valor = 0;
		List<Item> items = getItens();
		for(Item item:items) {
			valor = valor + (item.getPreco()*item.getQuantidade());
		}
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public List<Item> getItens() {
		return itens;
	}

	public void setItens(List<Item> itens) {
		this.itens = itens;
	}

	@Override
	public String toString() {
		return "NotaFiscal [id=" + id + ", numero=" + numero + ", modelo=" + modelo + ", dataOperacao=" + dataOperacao
				+ ", dataEmissao=" + dataEmissao + ", informacaoComplementar=" + informacaoComplementar + ", itens="
				+ itens + ", emitente=" + emitente + ", destinatario=" + destinatario + ", qtdItens=" + qtdItens
				+ ", valor=" + valor + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotaFiscal other = (NotaFiscal) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}