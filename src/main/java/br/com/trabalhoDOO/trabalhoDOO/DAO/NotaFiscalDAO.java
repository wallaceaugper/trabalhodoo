package br.com.trabalhoDOO.trabalhoDOO.DAO;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.trabalhoDOO.trabalhoDOO.model.NotaFiscal;
import br.com.trabalhoDOO.trabalhoDOO.utils.EntityManagerUtils;

public class NotaFiscalDAO {

	private EntityManager entityManager;

	private EntityManagerUtils entityManagerUtils = new EntityManagerUtils();

	public NotaFiscal salvar(NotaFiscal notaFiscal) throws SQLException {
		entityManager = entityManagerUtils.getEntityManager();

		entityManager.getTransaction().begin();

		if (notaFiscal.getId() == null) {
			entityManager.persist(notaFiscal);
		} else {
			entityManager.merge(notaFiscal);
		}

		entityManager.getTransaction().commit();

		return notaFiscal;
	}

	public void excluir(NotaFiscal notaFiscal) {
		entityManager = entityManagerUtils.getEntityManager();

		try {
			entityManager.getTransaction().begin();

			notaFiscal = entityManager.merge(notaFiscal);
//			System.out.println("Excluindo os dados de: " + notaFiscal.getNumero());

			entityManager.remove(notaFiscal);

			entityManager.getTransaction().commit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public NotaFiscal consultarPorId(Integer id) {
		entityManager = entityManagerUtils.getEntityManager();

		NotaFiscal notaFiscal = null;

		try {
//			notaFiscal = entityManager.find(NotaFiscal.class, id);
			notaFiscal = (NotaFiscal) entityManager.createQuery("FROM NotaFiscal WHERE numero = " + id)
					.getSingleResult();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return notaFiscal;
	}

	@SuppressWarnings("unchecked")
	public List<NotaFiscal> consultarTodasNotasFiscais() {
		entityManager = entityManagerUtils.getEntityManager();

		List<NotaFiscal> notasFiscais = new ArrayList<>();

		notasFiscais = entityManager.createQuery("FROM NotaFiscal").getResultList();

		return notasFiscais;
	}
	
	public Long totalNotas () {
		entityManager = entityManagerUtils.getEntityManager();

		return (Long) entityManager.createQuery("select count(*) from NotaFiscal").getSingleResult();
	}

}
