package br.com.trabalhoDOO.trabalhoDOO.controller;

import java.util.List;

import br.com.trabalhoDOO.trabalhoDOO.DAO.PessoaDAO;
import br.com.trabalhoDOO.trabalhoDOO.model.Pessoa;

public class PessoaController {

	private PessoaDAO pessoaDAO = new PessoaDAO();
	
	public void salvar(Pessoa pessoa) {
		pessoaDAO.salvar(pessoa);
	}
	
	public void excluir(Pessoa pessoa) {
		pessoaDAO.excluir(pessoa);
	}
	
	public Pessoa consultarPorId(Integer id) {
		return pessoaDAO.consultarPorId(id);
	}
	
	public List<Pessoa> consultarTodos() {
		return pessoaDAO.consultarTodos();
	}
	
}