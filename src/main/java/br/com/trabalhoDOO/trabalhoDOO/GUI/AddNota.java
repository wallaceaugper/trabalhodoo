package br.com.trabalhoDOO.trabalhoDOO.GUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JTextArea;
import javax.swing.JSeparator;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.JScrollPane;
import org.eclipse.wb.swing.FocusTraversalOnArray;

import br.com.trabalhoDOO.trabalhoDOO.controller.NotaFiscalController;
import br.com.trabalhoDOO.trabalhoDOO.model.Item;
import br.com.trabalhoDOO.trabalhoDOO.model.NotaFiscal;
import br.com.trabalhoDOO.trabalhoDOO.model.Pessoa;

import java.awt.Component;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.border.BevelBorder;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;

public class AddNota extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField notaNumero;
	private JTextField nota_dtEmissao;
	private JTextField notaModelo;
	private JTextField nota_dtOperacao;
	private JTable itemsTable;
	private JTextField itemCodigo;
	private JTextField itemPreco;
	private JTextField itemQtd;
	private JTextField destinatarioNome;
	private JTextField destinatarioInscricao;
	private JTextField destinatarioCPF;
	private JTextField destinatarioEstado;
	private JTextField emitenteNome;
	private JTextField emitenteInscricao;
	private JTextField emitenteCPF;
	private JTextField emitenteEstado;
	private NotaFiscalController notaFiscalController = new NotaFiscalController();
	private JTextArea nota_infComplementar;

	/**
	 * Create the dialog.
	 */
	public AddNota(NotaFiscal notaFiscal) {
		if (notaFiscal.getId() == null) {
			setTitle("Cadastrar Nota Fiscal");
		}else {
			setTitle("Editar Nota Fiscal");	
		}
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				if (notaFiscal.getId() != null) {
					notaNumero.setText(String.valueOf(notaFiscal.getNumero()));
					notaModelo.setText(notaFiscal.getModelo());
					nota_dtEmissao.setText(new SimpleDateFormat("dd/MM/yyyy").format(notaFiscal.getDataEmissao()));
					nota_dtOperacao.setText(new SimpleDateFormat("dd/MM/yyyy").format(notaFiscal.getDataOperacao()));
					getNota_infComplementar().append(notaFiscal.getInformacaoComplementar());
					
					Pessoa pessoa = notaFiscal.getDestinatario();
					destinatarioNome.setText(pessoa.getNome());
					destinatarioCPF.setText(pessoa.getCnpjCpf());
					destinatarioInscricao.setText(pessoa.getInscricaoEstadual());
					destinatarioEstado.setText(pessoa.getEstado());
					pessoa = notaFiscal.getEmitente();
					emitenteNome.setText(pessoa.getNome());
					emitenteCPF.setText(pessoa.getCnpjCpf());
					emitenteInscricao.setText(pessoa.getInscricaoEstadual());
					emitenteEstado.setText(pessoa.getEstado());
					
					List<Item> itens = notaFiscal.getItens();
			        DefaultTableModel model = (DefaultTableModel) itemsTable.getModel();
			        for (Item item:
			        	itens) {
			        	model.addRow(new Object[] {
			        			item.getCodigo(),
			        			item.getPreco(),
			        			item.getQuantidade(),
			        			item.getDescricao()
			        	});
			        }
				}
			}
		});
		setBounds(100, 100, 981, 673);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblNumero = new JLabel("Numero");
		lblNumero.setBounds(68, 8, 83, 14);
		contentPanel.add(lblNumero);
		
		notaNumero = new JTextField();
		notaNumero.setBounds(139, 5, 150, 20);
		contentPanel.add(notaNumero);
		notaNumero.setColumns(10);
		
		JLabel lblDataDeEmisso = new JLabel("Data de Emissão");
		lblDataDeEmisso.setBounds(28, 43, 123, 14);
		contentPanel.add(lblDataDeEmisso);
		
		nota_dtEmissao = new JTextField();
		nota_dtEmissao.setColumns(10);
		nota_dtEmissao.setBounds(139, 40, 150, 20);
		contentPanel.add(nota_dtEmissao);
		
		JLabel lblModelo = new JLabel("Modelo");
		lblModelo.setBounds(392, 14, 63, 14);
		contentPanel.add(lblModelo);
		
		notaModelo = new JTextField();
		notaModelo.setColumns(10);
		notaModelo.setBounds(477, 8, 150, 20);
		contentPanel.add(notaModelo);
		
		JLabel lblDataDaOperao = new JLabel("Data da Operação");
		lblDataDaOperao.setBounds(338, 49, 117, 14);
		contentPanel.add(lblDataDaOperao);
		
		nota_dtOperacao = new JTextField();
		nota_dtOperacao.setColumns(10);
		nota_dtOperacao.setBounds(477, 43, 150, 20);
		contentPanel.add(nota_dtOperacao);
		
		JLabel lblNewLabel = new JLabel("Informação Complementar");
		lblNewLabel.setBounds(5, 160, 163, 14);
		contentPanel.add(lblNewLabel);
		
		nota_infComplementar = new JTextArea();
		nota_infComplementar.setBounds(178, 100, 759, 146);
		contentPanel.add(nota_infComplementar);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(0, 296, 965, 2);
		contentPanel.add(separator);
		
		JLayeredPane layeredPane = new JLayeredPane();
		layeredPane.setBounds(5, 296, 950, 294);
		contentPanel.add(layeredPane);
		
		JPanel Pessoas = new JPanel();
		layeredPane.setLayer(Pessoas, 0);
		Pessoas.setBounds(0, 0, 950, 294);
		layeredPane.add(Pessoas);
		Pessoas.setLayout(null);
		
		JPanel destinatarioPanel = new JPanel();
		destinatarioPanel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		destinatarioPanel.setBounds(10, 11, 425, 221);
		Pessoas.add(destinatarioPanel);
		destinatarioPanel.setLayout(null);
		
		JLabel destinatarioNomeLabel = new JLabel("Nome");
		destinatarioNomeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		destinatarioNomeLabel.setBounds(62, 38, 87, 14);
		destinatarioPanel.add(destinatarioNomeLabel);
		
		destinatarioNome = new JTextField();
		destinatarioNome.setBounds(159, 35, 172, 20);
		destinatarioNome.setColumns(10);
		destinatarioPanel.add(destinatarioNome);
		
		JLabel destinatarioCPFLabel = new JLabel("CPF");
		destinatarioCPFLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		destinatarioCPFLabel.setBounds(70, 79, 79, 14);
		destinatarioPanel.add(destinatarioCPFLabel);
		
		destinatarioInscricao = new JTextField();
		destinatarioInscricao.setBounds(159, 122, 172, 20);
		destinatarioInscricao.setColumns(10);
		destinatarioPanel.add(destinatarioInscricao);
		
		destinatarioCPF = new JTextField();
		destinatarioCPF.setBounds(159, 76, 172, 20);
		destinatarioCPF.setColumns(10);
		destinatarioPanel.add(destinatarioCPF);
		
		JLabel destinatarioInscricaoLabel = new JLabel("Inscrição Estadual");
		destinatarioInscricaoLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		destinatarioInscricaoLabel.setBounds(10, 125, 139, 14);
		destinatarioPanel.add(destinatarioInscricaoLabel);
		
		JLabel destinatarioEstadoLabel = new JLabel("Estado");
		destinatarioEstadoLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		destinatarioEstadoLabel.setBounds(88, 178, 61, 14);
		destinatarioPanel.add(destinatarioEstadoLabel);
		
		destinatarioEstado = new JTextField();
		destinatarioEstado.setColumns(10);
		destinatarioEstado.setBounds(159, 175, 172, 20);
		destinatarioPanel.add(destinatarioEstado);
		
		JLabel destinatarioLabel = new JLabel("Destinatário");
		destinatarioLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		destinatarioLabel.setHorizontalAlignment(SwingConstants.CENTER);
		destinatarioLabel.setBounds(10, 7, 405, 17);
		destinatarioPanel.add(destinatarioLabel);
		
		JPanel emitentePanel = new JPanel();
		emitentePanel.setLayout(null);
		emitentePanel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		emitentePanel.setBounds(515, 11, 425, 221);
		Pessoas.add(emitentePanel);
		
		JLabel emitenteNomeLabel = new JLabel("Nome");
		emitenteNomeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		emitenteNomeLabel.setBounds(68, 35, 87, 14);
		emitentePanel.add(emitenteNomeLabel);
		
		emitenteNome = new JTextField();
		emitenteNome.setColumns(10);
		emitenteNome.setBounds(165, 32, 172, 20);
		emitentePanel.add(emitenteNome);
		
		JLabel emitenteCPFLabel = new JLabel("CPF");
		emitenteCPFLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		emitenteCPFLabel.setBounds(76, 76, 79, 14);
		emitentePanel.add(emitenteCPFLabel);
		
		emitenteInscricao = new JTextField();
		emitenteInscricao.setColumns(10);
		emitenteInscricao.setBounds(165, 122, 172, 20);
		emitentePanel.add(emitenteInscricao);
		
		emitenteCPF = new JTextField();
		emitenteCPF.setColumns(10);
		emitenteCPF.setBounds(165, 73, 172, 20);
		emitentePanel.add(emitenteCPF);
		
		JLabel emitenteInscricaoLabel = new JLabel("Inscrição Estadual");
		emitenteInscricaoLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		emitenteInscricaoLabel.setBounds(10, 125, 145, 14);
		emitentePanel.add(emitenteInscricaoLabel);
		
		JLabel emitenteEstadoLabel = new JLabel("Estado");
		emitenteEstadoLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		emitenteEstadoLabel.setBounds(94, 175, 61, 14);
		emitentePanel.add(emitenteEstadoLabel);
		
		emitenteEstado = new JTextField();
		emitenteEstado.setColumns(10);
		emitenteEstado.setBounds(165, 172, 172, 20);
		emitentePanel.add(emitenteEstado);
		
		JLabel emitenteLabel = new JLabel("Emitente");
		emitenteLabel.setHorizontalAlignment(SwingConstants.CENTER);
		emitenteLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		emitenteLabel.setBounds(10, 7, 405, 17);
		emitentePanel.add(emitenteLabel);
		
		JPanel Items = new JPanel();
		layeredPane.setLayer(Items, 1);
		Items.setBounds(0, 0, 950, 294);
		layeredPane.add(Items);
		Items.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 157, 950, 137);
		Items.add(scrollPane);
		
		itemsTable = new JTable();
		scrollPane.setViewportView(itemsTable);
		itemsTable.setColumnSelectionAllowed(true);
		itemsTable.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Codigo", "Pre\u00E7o", "Quantidade", "Descri\u00E7\u00E3o"
			}
		) {
			Class[] columnTypes = new Class[] {
				Integer.class, Double.class, Integer.class, String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		
		JLabel lblCodigo = new JLabel("Codigo");
		lblCodigo.setBounds(10, 28, 106, 14);
		Items.add(lblCodigo);
		
		itemCodigo = new JTextField();
		itemCodigo.setColumns(10);
		itemCodigo.setBounds(51, 25, 196, 20);
		Items.add(itemCodigo);
		
		JLabel lblPreo = new JLabel("Preço");
		lblPreo.setBounds(281, 28, 106, 14);
		Items.add(lblPreo);
		
		itemPreco = new JTextField();
		itemPreco.setColumns(10);
		itemPreco.setBounds(322, 25, 196, 20);
		Items.add(itemPreco);
		
		JLabel lblQuantidade = new JLabel("Quantidade");
		lblQuantidade.setBounds(562, 25, 106, 14);
		Items.add(lblQuantidade);
		
		itemQtd = new JTextField();
		itemQtd.setColumns(10);
		itemQtd.setBounds(631, 22, 196, 20);
		Items.add(itemQtd);
		
		JLabel lblNewLabel_1 = new JLabel("Descrição");
		lblNewLabel_1.setBounds(10, 92, 76, 14);
		Items.add(lblNewLabel_1);
		
		JTextArea itemDescricao = new JTextArea();
		itemDescricao.setLineWrap(true);
		itemDescricao.setBounds(87, 53, 300, 97);
		Items.add(itemDescricao);
		
		JButton btnAdicionar = new JButton("Adicionar");
		btnAdicionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model = (DefaultTableModel) itemsTable.getModel();
				model.addRow(new Object[] {Integer.parseInt(itemCodigo.getText()), Double.parseDouble(itemPreco.getText()), Integer.parseInt(itemQtd.getText()), itemDescricao.getText()});
			}
		});
		btnAdicionar.setBounds(429, 88, 89, 23);
		Items.add(btnAdicionar);
		
		JButton btnRemover = new JButton("Remover");
		btnRemover.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int viewIndex = itemsTable.getSelectedRow();
				int modelIndex = itemsTable.convertRowIndexToModel(viewIndex);
				DefaultTableModel model = (DefaultTableModel) itemsTable.getModel();
				model.removeRow(modelIndex);
			}
		});
		btnRemover.setBounds(528, 88, 89, 23);
		Items.add(btnRemover);
		Items.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{scrollPane, itemsTable, lblCodigo, itemCodigo}));
		layeredPane.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{Pessoas, Items}));
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(416, 258, 117, 27);
		contentPanel.add(comboBox);
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (comboBox.getSelectedIndex() == 0) {
					layeredPane.setLayer(Items, 1);
					layeredPane.setLayer(Pessoas, 0);
				}else if (comboBox.getSelectedIndex() == 1) {
					layeredPane.setLayer(Items, 0);
					layeredPane.setLayer(Pessoas, 1);
				}
			}
		});
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Items", "Pessoas"}));
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Salvar");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						try {
							SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
							Pessoa emitente;
							Pessoa destinatario;
							if (notaFiscal.getId() == null) {
								emitente = new Pessoa();
								destinatario = new Pessoa();
							} else {
								emitente = notaFiscal.getEmitente();
								destinatario = notaFiscal.getDestinatario();
							}
							
							notaFiscal.setNumero(Integer.parseInt(notaNumero.getText()));
							notaFiscal.setModelo(notaModelo.getText());
							notaFiscal.setDataEmissao(dateFormat.parse(nota_dtEmissao.getText()));
							notaFiscal.setDataOperacao(dateFormat.parse(nota_dtOperacao.getText()));
							notaFiscal.setInformacaoComplementar(nota_infComplementar.getText());
							
							emitente.setCnpjCpf(emitenteCPF.getText());
							emitente.setEstado(emitenteEstado.getText());
							emitente.setInscricaoEstadual(emitenteInscricao.getText());
							emitente.setNome(emitenteNome.getText());
							destinatario.setCnpjCpf(destinatarioCPF.getText());
							destinatario.setEstado(destinatarioEstado.getText());
							destinatario.setInscricaoEstadual(destinatarioInscricao.getText());
							destinatario.setNome(destinatarioNome.getText());
							
							notaFiscal.setEmitente(emitente);
							notaFiscal.setDestinatario(destinatario);
							
							List<Item> itens = new ArrayList<Item>();
							TableModel tableModel = itemsTable.getModel();
							for(int i=0; i<tableModel.getRowCount(); i++) {
								Item item = new Item();
								item.setCodigo((Integer) tableModel.getValueAt(i, 0));
								item.setPreco((Double) tableModel.getValueAt(i, 1));
								item.setQuantidade((Integer) tableModel.getValueAt(i, 2));
								item.setDescricao((String) tableModel.getValueAt(i, 3));
								item.setNotaFiscal(notaFiscal);
								
								itens.add(item);
							}
							notaFiscal.setItens(itens);
							
							notaFiscalController.salvar(notaFiscal);
							
							dispose();
						} catch (Exception e) {
							if (e.getMessage() == "Error while committing the transaction") {
								JOptionPane.showMessageDialog(new JFrame(), "Numero da nota fiscal já existe.", "Error",
								        JOptionPane.ERROR_MESSAGE);
							} else {
								JOptionPane.showMessageDialog(new JFrame(), e.getMessage(), "Error",
								        JOptionPane.ERROR_MESSAGE);
							}
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancelar");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
	public String getNotaNumero() {
		return notaNumero.getText();
	}
	protected JTextArea getNota_infComplementar() {
		return nota_infComplementar;
	}
}
