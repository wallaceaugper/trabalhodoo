package br.com.trabalhoDOO.trabalhoDOO.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerUtils {

	private ThreadLocal<EntityManager> entityManager = new ThreadLocal<>();
	
	private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("trabalhoDOO");

	public EntityManager getEntityManager() {

		if( entityManager.get() == null) {

			try {
				entityManager.set(entityManagerFactory.createEntityManager());
			} catch (Exception e) {
				entityManagerFactory.close();
				e.getMessage();
			}
		}
		return entityManager.get();
	}
}
