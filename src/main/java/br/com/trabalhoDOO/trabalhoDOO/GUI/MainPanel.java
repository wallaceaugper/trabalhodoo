package br.com.trabalhoDOO.trabalhoDOO.GUI;

import br.com.trabalhoDOO.trabalhoDOO.controller.NotaFiscalController;
import br.com.trabalhoDOO.trabalhoDOO.model.NotaFiscal;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.SimpleDateFormat;
import java.util.List;

public class MainPanel {

    private JFrame jFrame = new JFrame();
    private JPanel activePanel;

    private NotaFiscalController notaFiscalController = new NotaFiscalController();

    public MainPanel() {
        jFrame.setTitle("Notas Fiscais");
        jFrame.setLayout(new BorderLayout());

        jFrame.setJMenuBar(criaMenu());

        activePanel = criaHome(notaFiscalController.consultarTodasNotasFiscais());
        jFrame.add(activePanel);

        jFrame.setSize(1500, 500);
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setVisible(true);
    }

    private void changeActivePanel (JPanel jPanel) {
        jFrame.remove(activePanel);
        jFrame.add(jPanel);
        activePanel = jPanel;
        jFrame.revalidate();
        jFrame.repaint();
    }

    private JMenuBar criaMenu() {
        JMenuItem home = new JMenuItem("Home");
        JMenuItem indicadores = new JMenuItem("Indicadores");
        JMenuItem sobre = new JMenuItem("Sobre");
        JMenuItem sair = new JMenuItem("Sair");

        home.setMnemonic(KeyEvent.VK_H);
        indicadores.setMnemonic(KeyEvent.VK_I);
        sobre.setMnemonic(KeyEvent.VK_B);
        sair.setMnemonic(KeyEvent.VK_S);

        home.setToolTipText("Ir para Tela Principal");
        indicadores.setToolTipText("Ir para Tela de Indicadores");
        sobre.setToolTipText("Ir para Tela Sobre");
        sair.setToolTipText("Sair da Aplicação");

        home.addActionListener(e -> changeActivePanel(criaHome(notaFiscalController.consultarTodasNotasFiscais())));
        indicadores.addActionListener(e -> changeActivePanel(criaIndicadores()));
        sobre.addActionListener(e -> changeActivePanel(criaSobre()));
        sair.addActionListener(e -> System.exit(0));

        JMenu menu = new JMenu("Menu");

        menu.setMnemonic(KeyEvent.VK_M);

        menu.add(home);
        menu.add(indicadores);
        menu.add(sobre);
        menu.add(sair);

        JMenuBar menubar = new JMenuBar();
        menubar.add(menu);

        return menubar;
    }

    private JPanel criaHome(List<NotaFiscal> notasFiscais) {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout());
        jPanel.add(criaToolBar(), BorderLayout.PAGE_START);

        Object[] colunas = new String[]{
                "Número da nota",
                "Data da emissão",
                "CNPJ/CPF Emitente",
                "CNPJ/CPF Destinatário",
                "Razão Social/Nome Emitente",
                "Razão Social/Nome Destinatário",
                "Quantidade de Itens",
                "Valor"
        };

        Object[][] dados = new Object[notasFiscais.size()][colunas.length];

        int i=0;

        for (NotaFiscal nota:
             notasFiscais) {
            dados[i][0] = nota.getNumero();
            dados[i][1] = new SimpleDateFormat("dd/MM/yyyy").format(nota.getDataEmissao());
            dados[i][2] = nota.getEmitente().getCnpjCpf();
            dados[i][3] = nota.getDestinatario().getCnpjCpf();
            dados[i][4] = nota.getEmitente().getNome();
            dados[i][5] = nota.getDestinatario().getNome();
            dados[i][6] = nota.getItens().size();
            dados[i][7] = nota.getValor();
            i++;
        }

//        Object[][] dados = new Object[][]{
//                {"Rodrigo", 29L,"Masculino", "a", "a", "a", "a", "a", "a"},
//                {"Rodrigo", 29L,"Masculino", "a", "a", "a", "a", "a", "a"},
//                {"Rodrigo", 29L,"Masculino", "a", "a", "a", "a", "a", "a"},
//                {"Rodrigo", 29L,"Masculino", "a", "a", "a", "a", "a", "a"},
//                {"Rodrigo", 29L,"Masculino", "a", "a", "a", "a", "a", "a"},
//                {"Rodrigo", 29L,"Masculino", "a", "a", "a", "a", "a", "a"},
//                {"Rodrigo", 29L,"Masculino", "a", "a", "a", "a", "a", "a"},
//                {"Rodrigo", 29L,"Masculino", "a", "a", "a", "a", "a", "a"},
//                {"Rodrigo", 29L,"Masculino", "a", "a", "a", "a", "a", "a"},
//                {"Maria",30L,"Feminino", "a", "a", "a", "a", "a", "a"},
//                {"Maria",30L,"Feminino", "a", "a", "a", "a", "a", "a"},
//                {"Maria",30L,"Feminino", "a", "a", "a", "a", "a", "a"},
//                {"Maria",30L,"Feminino", "a", "a", "a", "a", "a", "a"},
//                {"Maria",30L,"Feminino", "a", "a", "a", "a", "a", "a"},
//                {"Maria",30L,"Feminino", "a", "a", "a", "a", "a", "a"},
//                {"Maria",30L,"Feminino", "a", "a", "a", "a", "a", "a"},
//                {"Maria",30L,"Feminino", "a", "a", "a", "a", "a", "a"},
//                {"Maria",30L,"Feminino", "a", "a", "a", "a", "a", "a"},
//        };

        jPanel.add(criaTable(colunas, dados));

        return jPanel;
    }

    private JPanel criaIndicadores () {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout());

        Object[] colunas = new String[]{
                "Indicador",
                "Valor"
        };
        
        String[] maiorEmitente = notaFiscalController.empresaMaiorNumero("emitente");
        String[] maiorDestinatario = notaFiscalController.empresaMaiorNumero("destinatario");

        Object[][] dados = new Object[][]{
                {"Total de Notas Fiscais cadastradas", notaFiscalController.consultarTotalNotas()},
                {"Média de valor das notas", notaFiscalController.mediaValorNotas()},
                {"Média de valor dos itens das notas", notaFiscalController.mediaValorItensNotas()},
                {"Maior valor de nota", notaFiscalController.maiorValorNota()},
                {"Estado com maior número de notas emitidas", notaFiscalController.estadoMaiorNumero("emitente")},
                {"Estado com maior número de notas como destinatário", notaFiscalController.estadoMaiorNumero("destinatario")},
                {"Maior compradora", maiorEmitente[0] +" - "+ maiorEmitente[1]},
                {"Maior vendedora", maiorDestinatario[0] +" - "+ maiorDestinatario[1]},
                {"Total de notas com valor superior a 10 mil", notaFiscalController.valorMaior10k()},
                {"Total de notas com mais de 10 itens", notaFiscalController.qtdItensMaior10()}
        };

        jPanel.add(criaTable(colunas, dados));

        return jPanel;
    }

    private JPanel criaSobre () {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout());

        JLabel jLabel = new JLabel();
        jLabel.setText("<html>" +
                "Disciplina:<br>"
                + "Desenv. Aplic. O.O."
                + "<br><br>" +
                "Equipe:<br>" +
                "Rafael Domingos de Oliveira<br>" +
                "Wallace Augusto Pereira de Moraes<br><br>" +
                "Informações sobre o Trabalho:<br>"
                + "Feito em Java (infelizmente) com mysql e jpa.<br>"
                + "Dá nota maxima pra nóiz, nuca pedimo nada. Best teachers ever.<br>"
                + "#ForaLovisi" +
                "</html>");

        jPanel.add(jLabel, BorderLayout.NORTH);

        return jPanel;
    }

    private JToolBar criaToolBar () {
        JButton adicionar = new JButton("Adicionar");
        JButton editar = new JButton("Editar");
        JButton remover = new JButton("Remover");

        adicionar.addActionListener(e -> addNota());
        editar.addActionListener(e -> editNota());
        remover.addActionListener(e -> removeRow());

        JToolBar toolbar = new JToolBar("Applications");
        toolbar.add(adicionar);
        toolbar.add(editar);
        toolbar.add(remover);

        toolbar.setFloatable(false);

        return toolbar;
    }

    private JScrollPane criaTable (Object[] colunas, Object[][] dados) {

        DefaultTableModel model = new DefaultTableModel(dados,colunas);
        JTable tabela = new JTable(model){
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false; //Disallow the editing of any cell
            }
        };

        JScrollPane  painelTabela = new JScrollPane();
        painelTabela.setViewportView(tabela);

        return painelTabela;
    }

    private void removeRow() {
        Component[] components = activePanel.getComponents();
        JScrollPane jScrollPane;
        JTable jTable = new JTable();
        for (Component component:
             components) {
            if (component instanceof JScrollPane) {
                jScrollPane = (JScrollPane) component;
                jTable = (JTable) jScrollPane.getViewport().getView();
            }
        }
        int viewIndex = jTable.getSelectedRow();
        if(viewIndex != -1) {
            int modelIndex = jTable.convertRowIndexToModel(viewIndex); // converts the row index in the view to the appropriate index in the model
            DefaultTableModel model = (DefaultTableModel)jTable.getModel();
            NotaFiscal nota = notaFiscalController.consultarPorId((Integer) model.getValueAt(modelIndex,0));
            notaFiscalController.excluir(nota);
            model.removeRow(modelIndex);
        }
    }

    private void addNota () {
    	
    	try {
			AddNota dialog = new AddNota(new NotaFiscal());
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosed(WindowEvent e) {
					changeActivePanel(criaHome(notaFiscalController.consultarTodasNotasFiscais()));
				}
			});
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}

    }
    
    private void editNota () {
    	Component[] components = activePanel.getComponents();
        JScrollPane jScrollPane;
        JTable jTable = new JTable();
        for (Component component:
             components) {
            if (component instanceof JScrollPane) {
                jScrollPane = (JScrollPane) component;
                jTable = (JTable) jScrollPane.getViewport().getView();
            }
        }
        int viewIndex = jTable.getSelectedRow();
        if(viewIndex != -1) {
            int modelIndex = jTable.convertRowIndexToModel(viewIndex); // converts the row index in the view to the appropriate index in the model
            DefaultTableModel model = (DefaultTableModel)jTable.getModel();
            NotaFiscal nota = notaFiscalController.consultarPorId((Integer) model.getValueAt(modelIndex,0));

            try {
    			AddNota dialog = new AddNota(nota);
    			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    			dialog.addWindowListener(new WindowAdapter() {
    				@Override
    				public void windowClosed(WindowEvent e) {
    					changeActivePanel(criaHome(notaFiscalController.consultarTodasNotasFiscais()));
    				}
    			});
    			dialog.setVisible(true);
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
        }
    }

}