package br.com.trabalhoDOO.trabalhoDOO.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import br.com.trabalhoDOO.trabalhoDOO.DAO.NotaFiscalDAO;
import br.com.trabalhoDOO.trabalhoDOO.model.Item;
import br.com.trabalhoDOO.trabalhoDOO.model.NotaFiscal;
import br.com.trabalhoDOO.trabalhoDOO.model.Pessoa;

public class NotaFiscalController {
	
	private NotaFiscalDAO notaFiscalDAO = new NotaFiscalDAO();
	
	public NotaFiscal salvar(NotaFiscal notaFiscal) throws SQLException  {
		return notaFiscalDAO.salvar(notaFiscal);
	}
	
	public void excluir(NotaFiscal notaFiscal) {
		notaFiscalDAO.excluir(notaFiscal);
	}
	
	public NotaFiscal consultarPorId(Integer id) {
		return notaFiscalDAO.consultarPorId(id);
	}
	
	public List<NotaFiscal> consultarTodasNotasFiscais() {
		return notaFiscalDAO.consultarTodasNotasFiscais();
	}
	
	public Long consultarTotalNotas() {
		return notaFiscalDAO.totalNotas();
	}
	
	public double mediaValorNotas() {
		List<NotaFiscal> notasFiscais = consultarTodasNotasFiscais();
		double valor = 0;
		int tamanho = notasFiscais.size();
		for (NotaFiscal notaFiscal : notasFiscais) {
			valor = valor + (notaFiscal.getValor());
		}
		return valor/tamanho;
	}
	
	public double mediaValorItensNotas() {
		List<NotaFiscal> notasFiscais = consultarTodasNotasFiscais();
		double valor = 0;
		int tamanho = 0;
		
		for (NotaFiscal notaFiscal : notasFiscais) {
			List<Item> items = notaFiscal.getItens();
			tamanho = tamanho + items.size();
			for (Item item : items) {
				valor = valor + item.getPreco();
			}
		}
		
		return valor/tamanho;
	}
	
	public double maiorValorNota() {
		double valor = 0;
		
		List<NotaFiscal> notasFiscais = consultarTodasNotasFiscais();
		
		for (NotaFiscal notaFiscal : notasFiscais) {
			double valorNota = notaFiscal.getValor();
			if (valor < valorNota) {
				valor = valorNota;
			}
		}
		
		return valor;
	}
	
	public String estadoMaiorNumero(String tipoPessoa) {
		String resultEstado = null;
		
		List<String> estados = new ArrayList<>();
		List<Integer> contagemEstados = new ArrayList<>();
		
		List<NotaFiscal> notasFiscais = consultarTodasNotasFiscais();
		
		for (NotaFiscal notaFiscal : notasFiscais) {
			String estado = null;
			if (tipoPessoa.equals("emitente")) {
				estado = notaFiscal.getEmitente().getEstado();
			}else if (tipoPessoa.equals("destinatario")) {
				estado = notaFiscal.getDestinatario().getEstado();
			}
			
			boolean achou = false; 
			for (int i = 0; i < estados.size(); i++) {
				if (estados.get(i).equals(estado)) {
					achou = true;
					contagemEstados.set(i, contagemEstados.get(i)+1);
				}
			}
			if (!achou) {
				estados.add(estado);
				contagemEstados.add(1);
			}
		}
		
		int maior=0;
		for (int i = 0; i < contagemEstados.size(); i++) {
			if (maior < contagemEstados.get(i)) {
				maior = contagemEstados.get(i);
				resultEstado = estados.get(i);
			}
		}
		
		return resultEstado;
	}
	
	public String[] empresaMaiorNumero(String tipoPessoa) {
		String[] pessoa = new String[2];
		
		List<String> empresaNome = new ArrayList<>();
		List<String> empresa = new ArrayList<>();
		List<Integer> contagemEmpresa = new ArrayList<>();
		
		List<NotaFiscal> notasFiscais = consultarTodasNotasFiscais();
		
		for (NotaFiscal notaFiscal : notasFiscais) {
			String cpf = null;
			String nome = null;
			if (tipoPessoa.equals("emitente")) {
				cpf = notaFiscal.getEmitente().getCnpjCpf();
				nome = notaFiscal.getEmitente().getNome();
			}else if (tipoPessoa.equals("destinatario")) {
				cpf = notaFiscal.getDestinatario().getCnpjCpf();
				nome = notaFiscal.getDestinatario().getNome();
			}
			
			boolean achou = false; 
			for (int i = 0; i < empresa.size(); i++) {
				if (empresa.get(i).equals(cpf)) {
					achou = true;
					contagemEmpresa.set(i, contagemEmpresa.get(i)+1);
				}
			}
			if (!achou) {
				empresa.add(cpf);
				empresaNome.add(nome);
				contagemEmpresa.add(1);
			}
		}
		
		int maior=0;
		for (int i = 0; i < contagemEmpresa.size(); i++) {
			if (maior < contagemEmpresa.get(i)) {
				maior = contagemEmpresa.get(i);
				pessoa[0] = empresa.get(i);
				pessoa[1] = empresaNome.get(i);
			}
		}
		
		return pessoa;
	}
	
	public int valorMaior10k() {
		int qtd=0;
		
		List<NotaFiscal> notasFiscais = consultarTodasNotasFiscais();
		
		for (NotaFiscal notaFiscal : notasFiscais) {
			if (notaFiscal.getValor() > 10000) {
				qtd++;
			}
		}
		
		return qtd;
	}
	
	public int qtdItensMaior10() {
		int qtd=0;
		
		List<NotaFiscal> notasFiscais = consultarTodasNotasFiscais();
		
		for (NotaFiscal notaFiscal : notasFiscais) {
			if (notaFiscal.getItens().size() > 10) {
				qtd++;
			}
		}
		
		return qtd;
	}

}