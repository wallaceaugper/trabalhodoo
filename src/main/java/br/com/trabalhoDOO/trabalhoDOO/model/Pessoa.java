package br.com.trabalhoDOO.trabalhoDOO.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "PESSOA")
public class Pessoa implements Serializable {

	private static final long serialVersionUID = 3445149624003038458L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idPessoa", nullable = false)
	private Integer id;
	
	@Column(name = "cnpjCpf", nullable = false, length = 14)
	private String cnpjCpf;
	
	@Column(name = "nome", nullable = false, length = 200)
	private String nome;
	
	@Column(name = "inscricaoEstadual", nullable = false, length = 20)
	private String inscricaoEstadual;
	
	@Column(name = "estado", nullable = false, length = 2)
	private String estado;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "emitente", cascade = CascadeType.ALL)
	private List<NotaFiscal> notasFiscalEmitente;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "destinatario", cascade = CascadeType.ALL)
	private List<NotaFiscal> notasFiscalDestinatario;

	public Pessoa(String cnpjCpf, String nome, String inscricaoEstadual, String estado) {
		this.cnpjCpf = cnpjCpf;
		this.nome = nome;
		this.inscricaoEstadual = inscricaoEstadual;
		this.estado = estado;
	}
	
	public Pessoa() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCnpjCpf() {
		return cnpjCpf;
	}

	public void setCnpjCpf(String cnpjCpf) {
		this.cnpjCpf = cnpjCpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public List<NotaFiscal> getNotasFiscalEmitente() {
		return notasFiscalEmitente;
	}

	public void setNotasFiscalEmitente(List<NotaFiscal> notasFiscalEmitente) {
		this.notasFiscalEmitente = notasFiscalEmitente;
	}

	public List<NotaFiscal> getNotasFiscalDestinatario() {
		return notasFiscalDestinatario;
	}

	public void setNotasFiscalDestinatario(List<NotaFiscal> notasFiscalDestinatario) {
		this.notasFiscalDestinatario = notasFiscalDestinatario;
	}
	
	@Override
	public String toString() {
		return "Pessoa [id=" + id + ", cnpjCpf=" + cnpjCpf + ", nome=" + nome + ", inscricaoEstadual="
				+ inscricaoEstadual + ", estado=" + estado + ", notasFiscalEmitente=" + notasFiscalEmitente
				+ ", notasFiscalDestinatario=" + notasFiscalDestinatario + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pessoa other = (Pessoa) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
